UPDATE pracownicy 
SET kurs='TESTER'
WHERE id IN (5, 10);
UPDATE pracownicy 
SET kurs=NULL
WHERE id BETWEEN 15 AND 20;

SELECT * FROM pracownicy;
WHERE imie='Anna';
SELECT * FROM pracownicy
WHERE imie IS NULL;
SELECT kurs FROM pracownicy
WHERE wiek BETWEEN 30 AND 40; 
SELECT * FROM pracownicy
WHERE nazwisko NOT LIKE '%and%';
SELECT wiek FROM pracownicy
WHERE id BETWEEN 1 AND 7;
SELECT * FROM pracownicy
WHERE wiek IS NULL;
ALTER TABLE pracownicy 
RENAME COLUMN kurs TO szkolenie;