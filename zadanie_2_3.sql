CREATE TABLE pracownicy (
id INTEGER PRIMARY KEY,
imie TEXT,
nazwisko TEXT,
wiek INTEGER CHECK (wiek>0),
kurs TEXT
);
INSERT INTO pracownicy (imie, nazwisko, wiek, kurs)
VALUES ('Anna', 'Nowak', 34, 'DS.');
INSERT INTO pracownicy (imie, nazwisko, wiek, kurs)
VALUES ('Roman', 'Kowalewski', 42, 'DS.');
INSERT INTO pracownicy (imie, nazwisko, wiek, kurs)
VALUES ('Tomasz', 'Wi�niewski', 33, 'DS.');
INSERT INTO pracownicy (imie, nazwisko, wiek, kurs)
VALUES ('Agata', 'W�jcik', 43, 'DS.');
INSERT INTO pracownicy (imie, nazwisko, wiek, kurs)
VALUES ('El�bieta', 'Kowalczyk', 28, 'Java');
INSERT INTO pracownicy (imie, nazwisko, wiek, kurs)
VALUES ('Przemys�aw', 'Kowalczyk', 34, 'Java');
INSERT INTO pracownicy (imie, nazwisko, wiek, kurs)
VALUES ('Robert', 'Kowalczyk', 35, 'Java');
INSERT INTO pracownicy (imie, nazwisko, wiek, kurs)
VALUES ('Rados�aw', 'Zieli�ski', 38, 'Java');
INSERT INTO pracownicy (nazwisko, wiek, kurs)
VALUES ('Wo�niak', 26, 'Java');
INSERT INTO pracownicy (imie, nazwisko, wiek, kurs)
VALUES ('Robert', 'Szyma�ski', 34, 'Java');
INSERT INTO pracownicy (imie, nazwisko, wiek)
VALUES ('Rados�aw', 'D�browski', 35);
INSERT INTO pracownicy (imie, nazwisko, kurs)
VALUES ('Robert', 'Koz�owski', 'UX');
INSERT INTO pracownicy (imie, nazwisko, wiek, kurs)
VALUES ('Joanna', 'Mazur', 26, 'UX');
INSERT INTO pracownicy (imie, nazwisko, wiek, kurs)
VALUES ('Rados�aw', 'Jankowski', 27, 'UX');
INSERT INTO pracownicy (imie, nazwisko, wiek, kurs)
VALUES ('Patryk', 'Lewandowski', 28, 'Tester');
INSERT INTO pracownicy (nazwisko, wiek, kurs)
VALUES ('Zieli�ski', 28, 'Tester');
INSERT INTO pracownicy (imie, nazwisko, wiek, kurs)
VALUES ('Andrzej', 'Wo�niak', 31, 'Tester');
INSERT INTO pracownicy (imie, nazwisko, wiek, kurs)
VALUES ('Andrzej', 'Lewandowski', 30, 'Tester');
INSERT INTO pracownicy (imie, nazwisko, wiek, kurs)
VALUES ('Roman', 'Kowalczyk', 39, 'Tester');
INSERT INTO pracownicy (imie, nazwisko, wiek, kurs)
VALUES ('Ewa', 'Wo�niak', 31, 'Tester');
SELECT * FROM pracownicy
WHERE nazwisko='Kowalczyk';
SELECT * FROM pracownicy
WHERE wiek BETWEEN 30 AND 40
ORDER BY wiek;
SELECT * FROM pracownicy;
WHERE nazwisko NOT LIKE '%and%';
SELECT * FROM pracownicy
WHERE id BETWEEN 1 AND 7;
SELECT * FROM pracownicy
WHERE imie IS NULL OR nazwisko IS NULL OR wiek IS NULL OR kurs IS NULL;
SELECT * FROM pracownicy
WHERE kurs IS NULL;
