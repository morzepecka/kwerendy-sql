UPDATE pracownicy 
SET imie='Anna'
WHERE id IN (4,6);
UPDATE pracownicy 
SET nazwisko=NULL
WHERE id=4;
UPDATE pracownicy 
SET wiek=NULL
WHERE id BETWEEN 5 AND 9;
UPDATE pracownicy 
SET imie='Robert'
WHERE id=9;
ALTER TABLE pracownicy 
RENAME COLUMN szkolenie TO kurs;

SELECT * FROM pracownicy;
/* unikatowe wiersze posortawane po imieniu*/
SELECT DISTINCT * FROM pracownicy
ORDER BY imie;
/* unikatowe imiona posortowane*/
SELECT DISTINCT imie FROM pracownicy
ORDER BY imie;
/* unikatowe wiersze posortawane po nazwisku*/
SELECT DISTINCT * FROM pracownicy
ORDER BY nazwisko;
/* unikatowe nazwiska posortowane*/
SELECT DISTINCT nazwisko FROM pracownicy
ORDER BY nazwisko;
SELECT DISTINCT kurs FROM pracownicy
WHERE nazwisko='Kowalczyk';
SELECT * FROM pracownicy 
WHERE wiek IS NULL;
SELECT wiek FROM pracownicy 
WHERE imie='Patryk';
ALTER TABLE pracownicy 
RENAME TO mentorzy;




